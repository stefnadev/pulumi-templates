# Pulumi templates

Custom Pulumi templates for setting up projects

## Templates

Details about the templates

### S3 static website behind CloudFront

Create a new project from the template

```shell
pulumi new https://bitbucket.org/stefnadev/pulumi-templates/templates/s3-cloudfront -s <org>/<project>/<stack>
# Example
pulumi new https://bitbucket.org/stefnadev/pulumi-templates/templates/s3-cloudfront -s stefna/some-quasar-project/static-site
```

## ToDo / Ideas

- [ ] Bitbucket API (deploy repository variables etc...)
- [ ] Add redirects functionality to the `s3-cloudfront` template
