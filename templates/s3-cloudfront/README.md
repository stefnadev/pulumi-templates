# S3 bucket, static website, behind CloudFront

A Pulumi template for deploying a S3 bucket (static website hosting) behind CloudFront.

## Setup procedure

1. Create a Pulumi project with this template
2. Set the relevant config
3. Run `pulumi up`

## Config

All you need to do is to set everything in the config for your desired stack.

Everything use the scope/key `project`

Set the necessary config... only the tags use for the AWS resources are set with config everything else is done in index.ts

```shell
pulumi config set --path project:tags.Jira <TICKET-1>
pulumi config set --path project:tags.CreatedBy pulumi
pulumi config set --path project:tags.Author "${USER}@stefna.is"
```

Config example:

```yaml
config:
  project:tags:
    Jira: STEFNA-1
    CreatedBy: pulumi
    Author: haukur@stefna.is
```

## Create a stage

It's a common use case to create a staging version and a production one, that's really easy by adding a stage in the
main [index.ts](index.ts) file, there you supply; the name of the stage, domain name, boolean if the AWS ACM is ready
for Pulumi to verify it, an S3 bucket and the basic config.

```typescript
// Create a production site
const production = createStage(
	'production',
	'example.stefna.is',
	// validateCertificate: set true when DNS verification has been completed,
	// in order to use the domain in CloudFront instance
	false,
	s3bucket.instance,
	config,
);
```

## Output

The output depends on how many stages you create, don't forget to specify them in the output as well, for example:

```typescript
export const STAGE_PROD = production.outputs;
```

We're going to need the output from the setup in order to configure the Bitbucket pipeline.

```shell
pulumi stack output --show-secrets
```

## Create a new stack

Common behaviour/environments, like staging and production, has been consolidated within one stack in order to be able
to reuse the S3 bucket for each stage. Keep that when creating the stack name, instead of using environment names like
`production` you should use more descriptive like `stefna/quasar` or `stefna/static-site`

```shell
pulumi stack init stefna/static-site
```

## Code style

This project uses eslint to enforce a specific code style, because Tabs vs. Spaces is always a fun debate :)
