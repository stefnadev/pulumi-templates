import config from './src/Config';
import createIamUser from './src/iamUser';
import createS3Bucket from './src/s3Bucket';
import createStage from './src/Stage';

// Create the S3 bucket static website (contains all stages within sub directories)
const s3bucket = createS3Bucket(config);

// Create a staging site
const staging = createStage(
	'staging',
	'example.staging.stefna.is',
	// validateCertificate: set true when DNS verification has been completed,
	// in order to use the domain in CloudFront instance
	false,
	s3bucket.instance,
	config,
);

// Create a production site
const production = createStage(
	'production',
	'example.stefna.is',
	// validateCertificate: set true when DNS verification has been completed,
	// in order to use the domain in CloudFront instance
	false,
	s3bucket.instance,
	config,
);

// AWS IAM user, with limited permissions, for the CI/CD pipeline
const iamUser = createIamUser(
	config,
	s3bucket.instance,
	[
		staging.distribution,
		production.distribution,
	],
);

// Export relevant details
export const AWS_DEFAULT_REGION = config.aws.region;
export const AWS_ACCESS_KEY_ID = iamUser.accessKey.id;
export const AWS_SECRET_ACCESS_KEY = iamUser.accessKey.secret;
export const S3_BUCKET = s3bucket.instance.id;
export const STAGE_STAGING = staging.outputs ;
export const STAGE_PROD = production.outputs;
