import * as aws from '@pulumi/aws';
import * as pulumi from '@pulumi/pulumi';
import config, { ConfigInterface } from './Config';

// Certificates for CloudFront must be in the 'us-east-1' region, so need to specify the AWS region
const awsUsEast = new aws.Provider('aws-provider-us-east-1', {
	region: 'us-east-1',
	profile: config.aws.profile,
});

interface AcmCert {
	certificate: aws.acm.Certificate,
	certificateValidation: aws.acm.CertificateValidation | undefined,
}

const createAcmCert = (
	domain: string,
	validateCertificate: boolean,
	config: ConfigInterface,
): AcmCert => {
	const cert = new aws.acm.Certificate(`${config.projectName}-${domain}-cert`, {
		domainName: domain,
		validationMethod: 'DNS',
		tags: config.tags,
	}, { provider: awsUsEast });

	let certificateValidation = undefined;

	if (validateCertificate) {
		certificateValidation = new aws.acm.CertificateValidation(`${config.projectName}-${domain}-certValidation`, {
			certificateArn: cert.arn,
		}, { provider: awsUsEast });
	} else {
		pulumi.all([cert.domainValidationOptions[0], cert.arn])
			.apply(([certOptions, arn]) => {
				const message = [
					'Please implement DNS record to verify ACM certification:',
					`\t${certOptions.resourceRecordName} IN ${certOptions.resourceRecordType} ${certOptions.resourceRecordValue}`,
					'Then toggle the "validateCertificate" boolean to enable the the certificate:',
					`\t${arn}`,
				];
				pulumi.log.info(message.join('\n'));
			});

	}

	return {
		certificate: cert,
		certificateValidation,
	};
};

export default createAcmCert;
