import * as pulumi from '@pulumi/pulumi';
import * as aws from '@pulumi/aws';
import { ConfigInterface } from './Config';

// Policy name: Managed-CachingOptimized
const cachePolicyId = '658327ea-f89d-4fab-a63d-7e88639e58f6';

export interface CloudFrontDistribution {
	instance: aws.cloudfront.Distribution,
	stage: string,
}

const createDistribution = (
	stage: string,
	domain: string,
	config: ConfigInterface,
	s3bucket: aws.s3.Bucket,
	acmArn: aws.acm.CertificateValidation | undefined,
): CloudFrontDistribution => {
	// service-code.region-code.amazonaws.com
	const originDomainName = pulumi.all([s3bucket.id, s3bucket.region, stage])
		.apply(([bucketId, bucketRegion]) => `${bucketId}.s3-website-${bucketRegion}.amazonaws.com`);

	const distributionArgs: aws.cloudfront.DistributionArgs = {
		comment: domain,
		tags: config.tags,
		defaultCacheBehavior: {
			allowedMethods: [
				'GET',
				'HEAD',
			],
			cachedMethods: [
				'GET',
				'HEAD',
			],
			targetOriginId: domain,
			viewerProtocolPolicy: 'redirect-to-https',
			compress: true,
			cachePolicyId,
		},
		enabled: true,
		isIpv6Enabled: true,
		aliases: [],
		origins: [
			{
				domainName: originDomainName,
				originId: domain,
				originPath: `/${stage}`,
				customHeaders: [
					{
						name: 'Referer',
						value: config.s3CloudFrontSecret,
					},
				],
				customOriginConfig: {
					httpPort: 80,
					httpsPort: 443,
					originProtocolPolicy: 'http-only',
					originSslProtocols: ['SSLv3'],
				},
			},
		],
		restrictions: {
			geoRestriction: {
				restrictionType: 'none',
			},
		},
		viewerCertificate: {
			cloudfrontDefaultCertificate: true,
		},
	};

	if (acmArn) {
		distributionArgs.aliases = [domain];
		distributionArgs.viewerCertificate = {
			acmCertificateArn: acmArn.certificateArn,
			cloudfrontDefaultCertificate: false,
			minimumProtocolVersion: 'TLSv1.2_2021',
			sslSupportMethod: 'sni-only',
		};
	}

	const instance = new aws.cloudfront.Distribution(`${config.projectName}-${stage}-distribution`, distributionArgs);

	return {
		instance,
		stage,
	};
};

export default createDistribution;
