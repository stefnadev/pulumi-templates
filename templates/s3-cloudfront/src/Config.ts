import * as pulumi from '@pulumi/pulumi';
import * as crypto from 'crypto';

interface AWS {
	region: string,
	profile: string | undefined,
}

interface Tags {
	[key: string]: string,
}

export interface ConfigInterface {
	projectName: string,
	s3CloudFrontSecret: string,
	tags: Tags,
	aws: AWS,
}

const projectConfig = new pulumi.Config('project');
const awsConfig = new pulumi.Config('aws');

const projectName = pulumi.getProject().toString() + '-' + pulumi.getStack().toString();

const s3CloudFrontSecret = crypto.createHash('sha256').update(projectName).digest('hex');
const s3CloudFrontSecretBase64 = Buffer.from(s3CloudFrontSecret, 'utf8').toString('base64');

const config: ConfigInterface = {
	projectName,
	s3CloudFrontSecret: s3CloudFrontSecretBase64,
	tags: projectConfig.requireObject<Tags>('tags'),
	aws: {
		region: awsConfig.require<string>('region'),
		profile: awsConfig.get<string>('profile'),
	},
};

export default config;
