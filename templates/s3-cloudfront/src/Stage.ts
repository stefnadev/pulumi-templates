import createAcmCert from './Acm';
import { ConfigInterface } from './Config';
import * as aws from '@pulumi/aws';
import createDistribution, { CloudFrontDistribution } from './Distribution';
import * as pulumi from '@pulumi/pulumi';

interface Stage {
	name: string,
	domain: string,
	outputs: {
		DOMAIN: string,
		S3_BUCKET: pulumi.Output<string>,
		DISTRIBUTION_ID: pulumi.Output<string>,
		DISTRIBUTION_URL: pulumi.Output<string>,
	}
	certificate: aws.acm.Certificate | undefined,
	certificateValidation: aws.acm.CertificateValidation | undefined,
	distribution: CloudFrontDistribution,
}

const createStage = (
	name: string,
	domain: string,
	validateCertificate: boolean,
	s3bucket: aws.s3.Bucket,
	config: ConfigInterface,
	skipCreateAcmCert = false,
): Stage => {
	const acm = skipCreateAcmCert ? {
		certificate: undefined,
		certificateValidation: undefined,
	} : createAcmCert(domain, validateCertificate, config);

	const distribution = createDistribution(name, domain, config, s3bucket, acm.certificateValidation);

	return {
		name,
		domain,
		outputs: {
			DOMAIN: domain,
			S3_BUCKET: s3bucket.id.apply((id) => `${id}/${name}`),
			DISTRIBUTION_ID: distribution.instance.id,
			DISTRIBUTION_URL: distribution.instance.domainName,
		},
		certificate: acm.certificate,
		certificateValidation: acm.certificateValidation,
		distribution,
	};
};

export default createStage;
