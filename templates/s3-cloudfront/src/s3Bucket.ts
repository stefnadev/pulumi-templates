import * as aws from '@pulumi/aws';
import { ConfigInterface } from './Config';

interface s3Bucket {
	instance: aws.s3.Bucket,
	secretPolicy: aws.s3.BucketPolicy,
}

const createS3Bucket = (
	config: ConfigInterface,
): s3Bucket => {

	// Create an AWS resource (S3 Bucket)
	const bucket = new aws.s3.Bucket(`${config.projectName}`, {
		website: {
			indexDocument: 'index.html',
		},
		forceDestroy: true,
		tags: config.tags,
	});

	new aws.s3.BucketPublicAccessBlock(`${config.projectName}-PublicAccessBlock`, {
		bucket: bucket.id,
		blockPublicAcls: false,
	});

	const policy = new aws.s3.BucketPolicy(`${config.projectName}-AllowAccessWithSecretKey`, {
		bucket: bucket.id,
		policy: {
			Version: '2008-10-17',
			Id: 'PolicyForCloudFrontOnlyToS3StaticWebsite',
			Statement: [
				{
					Sid: '1',
					Effect: 'Allow',
					Principal: '*',
					Action: 's3:GetObject',
					Resource: bucket.arn.apply(arn => arn.toString() + '/*'),
					Condition: {
						StringLike: {
							'aws:Referer': config.s3CloudFrontSecret,
						},
					},
				},
			],
		},
	});

	return {
		instance: bucket,
		secretPolicy: policy,
	};
};

export default createS3Bucket;
