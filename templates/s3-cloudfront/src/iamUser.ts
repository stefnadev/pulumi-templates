import * as aws from '@pulumi/aws';
import { ConfigInterface } from './Config';
import { CloudFrontDistribution } from './Distribution';

interface User {
	instance: aws.iam.User,
	accessKey: aws.iam.AccessKey,
	s3Policy: aws.iam.UserPolicy,
	cloudfrontPolicies: aws.iam.UserPolicy[],
}

const createIamUser = (
	config: ConfigInterface,
	bucket: aws.s3.Bucket,
	distributions: CloudFrontDistribution[],
): User => {
	const user = new aws.iam.User(`ci-${config.projectName}`, {
		path: '/system/',
		tags: config.tags,
	});
	const accessKey = new aws.iam.AccessKey(`${config.projectName}-lbAccessKey`, {
		user: user.name,
	});

	const s3Policy = new aws.iam.UserPolicy(`${config.projectName}-AllowAccessToS3Bucket`, {
		user: user.name,
		policy: {
			Version: '2012-10-17',
			Statement: [
				{
					Sid: 'VisualEditor0',
					Effect: 'Allow',
					Action: [
						's3:PutObject',
						's3:GetObject',
						's3:ListBucket',
						's3:DeleteObject',
					],
					Resource: [
						bucket.arn.apply(arn => arn.toString() + '/*'),
						bucket.arn,
					],
				},
			],
		},
	});

	const cloudfrontPolicies = [];

	for (const distribution of distributions) {
		const stageName = distribution.stage.replace('/', '-');
		cloudfrontPolicies.push(new aws.iam.UserPolicy(`${config.projectName}-${stageName}-AllowCloudFrontCreateInvalidation`, {
			user: user.name,
			policy: {
				Version: '2012-10-17',
				Statement: [
					{
						Sid: 'VisualEditor0',
						Effect: 'Allow',
						Action: 'cloudfront:CreateInvalidation',
						Resource: distribution.instance.arn,
					},
				],
			},
		}));
	}

	return {
		instance: user,
		accessKey,
		s3Policy,
		cloudfrontPolicies,
	};
};

export default createIamUser;
